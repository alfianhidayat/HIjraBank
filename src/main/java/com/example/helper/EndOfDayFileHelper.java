package com.example.helper;

import com.example.contants.HeaderConstants;

import static com.example.contants.FileConstants.SEMICOLON;

public class EndOfDayFileHelper {
    public static String createHeader() {
        StringBuilder builder = new StringBuilder();
        builder.append(HeaderConstants.ID);
        builder.append(SEMICOLON);
        builder.append(HeaderConstants.NAME);
        builder.append(SEMICOLON);
        builder.append(HeaderConstants.AGE);
        builder.append(SEMICOLON);
        builder.append(HeaderConstants.BALANCED);
        builder.append(SEMICOLON);
        builder.append(HeaderConstants.THREAD_2B);
        builder.append(SEMICOLON);
        builder.append(HeaderConstants.THREAD_3);
        builder.append(SEMICOLON);
        builder.append(HeaderConstants.PREVIOUS_BALANCED);
        builder.append(SEMICOLON);
        builder.append(HeaderConstants.AVERAGE_BALANCED);
        builder.append(SEMICOLON);
        builder.append(HeaderConstants.THREAD_1);
        builder.append(SEMICOLON);
        builder.append(HeaderConstants.FREE_TRANSFER);
        builder.append(SEMICOLON);
        builder.append(HeaderConstants.THREAD_2A);
        return builder.toString();
    }
}
