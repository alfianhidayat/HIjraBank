package com.example.helper;

public interface MapperHelper<L, R> {

    L from(R value);

    R convert(L value);

}
