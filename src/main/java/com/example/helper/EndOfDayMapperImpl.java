package com.example.helper;

import com.example.data.EndOfDay;
import com.example.contants.IndexConstants;

import static com.example.contants.FileConstants.SEMICOLON;

public class EndOfDayMapperImpl implements MapperHelper<EndOfDay, String> {

    @Override
    public EndOfDay from(String value) {
        String[] data = value.split(SEMICOLON);
        EndOfDay endOfDay = new EndOfDay();
        endOfDay.setId(Integer.parseInt(data[IndexConstants.ID]));
        endOfDay.setName(data[IndexConstants.NAME]);
        endOfDay.setAge(Integer.parseInt(data[IndexConstants.AGE]));
        endOfDay.setBalanced(Long.parseLong(data[IndexConstants.BALANCED]));
        endOfDay.setPreviousBalanced(Long.parseLong(data[IndexConstants.PREVIOUS_BALANCED]));
        endOfDay.setFreeTransfer(Long.parseLong(data[IndexConstants.FREE_TRANSFER]));
        return endOfDay;
    }

    @Override
    public String convert(EndOfDay value) {
        StringBuilder builder = new StringBuilder();
        builder.append(value.getId());
        builder.append(SEMICOLON);
        builder.append(value.getName());
        builder.append(SEMICOLON);
        builder.append(value.getAge());
        builder.append(SEMICOLON);
        builder.append(value.getBalanced());
        builder.append(SEMICOLON);
        builder.append(value.getThreadNo2b());
        builder.append(SEMICOLON);
        builder.append(value.getThreadNo3());
        builder.append(SEMICOLON);
        builder.append(value.getPreviousBalanced());
        builder.append(SEMICOLON);
        builder.append(value.getAverageBalanced());
        builder.append(SEMICOLON);
        builder.append(value.getThreadNo1());
        builder.append(SEMICOLON);
        builder.append(value.getFreeTransfer());
        builder.append(SEMICOLON);
        builder.append(value.getThreadNo2a());
        return builder.toString();
    }
}
