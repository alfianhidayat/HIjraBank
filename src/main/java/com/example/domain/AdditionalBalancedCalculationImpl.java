package com.example.domain;

import com.example.data.EndOfDay;

public class AdditionalBalancedCalculationImpl implements EndOfDayCalculation {

    private static final int NUMBER_OF_THREADS = 8;
    private static final int LIMIT = 100;
    private static final int BONUS = 10;

    @Override
    public EndOfDay calculate(EndOfDay endOfDay) {
        long balanced = endOfDay.getBalanced();
        if (endOfDay.getId() <= LIMIT) {
            endOfDay.setBalanced(balanced + BONUS);
        }
        endOfDay.setThreadNo3(Thread.currentThread().getName());
        return endOfDay;
    }

    @Override
    public int getNumberOfThreads() {
        return NUMBER_OF_THREADS;
    }
}
