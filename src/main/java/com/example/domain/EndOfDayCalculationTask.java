package com.example.domain;

import com.example.contants.FileConstants;
import com.example.data.EndOfDay;

import java.io.File;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

public class EndOfDayCalculationTask {

    private final FileProcessorService<EndOfDay> fileProcessorService;

    public EndOfDayCalculationTask(FileProcessorService<EndOfDay> fileProcessorService) {
        this.fileProcessorService = fileProcessorService;
    }

    public void execute() throws Exception {
        File beforeEodFile = new File(FileConstants.BEFORE_EOD_FILE_PATH);
        List<EndOfDay> beforeEndOfDays = this.fileProcessorService.readFile(beforeEodFile);

        this.calculate(beforeEndOfDays, new AverageBalancedCalculationImpl());
        this.calculate(beforeEndOfDays, new FreeTransferCalculationImpl());
        this.calculate(beforeEndOfDays, new BenefitBalancedCalculationImpl());
        this.calculate(beforeEndOfDays, new AdditionalBalancedCalculationImpl());

        File afterEodFile = new File(FileConstants.AFTER_EOD_FILE_PATH);
        this.fileProcessorService.writeFile(beforeEndOfDays, afterEodFile);
    }

    private void calculate(List<EndOfDay> endOfDays, EndOfDayCalculation calculation) {
        ExecutorService executorService = calculation.getExecutorService();
        try {
            List<Future<EndOfDay>> futures = endOfDays.stream()
                    .map(item -> executorService.submit(() -> calculation.calculate(item)))
                    .collect(Collectors.toList());
            for (Future<EndOfDay> future : futures) {
                future.get();
            }
        } catch (InterruptedException | ExecutionException e) {
            throw new RuntimeException(e);
        } finally {
            if (executorService != null) {
                executorService.shutdown();
            }
        }
    }

}
