package com.example.domain;

import com.example.data.EndOfDay;

public class BenefitBalancedCalculationImpl implements EndOfDayCalculation {

    private static final int THRESHOLD = 150;
    private static final int BENEFIT = 25;

    @Override
    public EndOfDay calculate(EndOfDay endOfDay) {
        long balanced = endOfDay.getBalanced();
        if (balanced > THRESHOLD) {
            endOfDay.setBalanced(balanced + BENEFIT);
        }
        endOfDay.setThreadNo2b(Thread.currentThread().getName());
        return endOfDay;
    }
}
