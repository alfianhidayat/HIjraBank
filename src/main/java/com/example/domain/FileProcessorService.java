package com.example.domain;

import java.io.File;
import java.util.List;

public interface FileProcessorService<T> {

    List<T> readFile(File file) throws Exception;

    File writeFile(List<T> values, File destFile) throws Exception;

}
