package com.example.domain;

import com.example.data.EndOfDay;

public class AverageBalancedCalculationImpl implements EndOfDayCalculation {

    @Override
    public EndOfDay calculate(EndOfDay endOfDay) {
        double average = endOfDay.getPreviousBalanced() + endOfDay.getBalanced() / (double) 2;
        endOfDay.setAverageBalanced(average);
        endOfDay.setThreadNo1(Thread.currentThread().getName());
        return endOfDay;
    }
}
