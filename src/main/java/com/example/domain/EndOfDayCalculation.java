package com.example.domain;

import com.example.data.EndOfDay;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public interface EndOfDayCalculation {

    int DEFAULT_THREADS = 4;

    EndOfDay calculate(EndOfDay endOfDay);

    default int getNumberOfThreads() {
        return DEFAULT_THREADS;
    }

    default ExecutorService getExecutorService() {
        return Executors.newFixedThreadPool(getNumberOfThreads());
    }

}
