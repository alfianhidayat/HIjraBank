package com.example.domain;

import com.example.data.EndOfDay;
import com.example.helper.EndOfDayFileHelper;
import com.example.helper.MapperHelper;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;

public class EndOfDayFileProcessorServiceImpl implements FileProcessorService<EndOfDay> {

    private final MapperHelper<EndOfDay, String> mapper;

    public EndOfDayFileProcessorServiceImpl(MapperHelper<EndOfDay, String> mapper) {
        this.mapper = mapper;
    }

    @Override
    public List<EndOfDay> readFile(File file) throws Exception {
        List<EndOfDay> endOfDays = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            String line = br.readLine(); //Skip Header
            while ((line = br.readLine()) != null) {
                endOfDays.add(mapper.from(line));
            }
        }
        return endOfDays;
    }

    @Override
    public File writeFile(List<EndOfDay> endOfDays, File destFile) throws Exception {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(destFile))) {
            writer.write(EndOfDayFileHelper.createHeader());
            for (EndOfDay endOfDay : endOfDays) {
                writer.newLine();
                writer.write(mapper.convert(endOfDay));
            }
        }
        return destFile;
    }
}
