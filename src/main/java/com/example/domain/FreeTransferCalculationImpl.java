package com.example.domain;

import com.example.data.EndOfDay;

public class FreeTransferCalculationImpl implements EndOfDayCalculation {

    private static final int MIN_THRESHOLD = 100;
    private static final int MAX_THRESHOLD = 150;
    private static final int BENEFIT = 5;

    @Override
    public EndOfDay calculate(EndOfDay endOfDay) {
        long balanced = endOfDay.getBalanced();
        if (balanced >= MIN_THRESHOLD && balanced <= MAX_THRESHOLD) {
            endOfDay.setFreeTransfer(BENEFIT);
        }
        endOfDay.setThreadNo2a(Thread.currentThread().getName());
        return endOfDay;
    }
}
