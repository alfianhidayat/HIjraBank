package com.example.data;

public class EndOfDay extends ThreadInfo {
    private int id;
    private String name;
    private int age;
    private long balanced;
    private long previousBalanced;
    private double averageBalanced;
    private long freeTransfer;

    public EndOfDay() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public long getBalanced() {
        return balanced;
    }

    public void setBalanced(long balanced) {
        this.balanced = balanced;
    }

    public long getPreviousBalanced() {
        return previousBalanced;
    }

    public void setPreviousBalanced(long previousBalanced) {
        this.previousBalanced = previousBalanced;
    }

    public double getAverageBalanced() {
        return averageBalanced;
    }

    public void setAverageBalanced(double averageBalanced) {
        this.averageBalanced = averageBalanced;
    }

    public long getFreeTransfer() {
        return freeTransfer;
    }

    public void setFreeTransfer(long freeTransfer) {
        this.freeTransfer = freeTransfer;
    }
}
