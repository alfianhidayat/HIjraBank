package com.example.data;

public class ThreadInfo {
    private String threadNo1;
    private String threadNo2a;
    private String threadNo2b;
    private String threadNo3;

    public ThreadInfo() {
    }

    public String getThreadNo1() {
        return threadNo1;
    }

    public void setThreadNo1(String threadNo1) {
        this.threadNo1 = threadNo1;
    }

    public String getThreadNo2a() {
        return threadNo2a;
    }

    public void setThreadNo2a(String threadNo2a) {
        this.threadNo2a = threadNo2a;
    }

    public String getThreadNo2b() {
        return threadNo2b;
    }

    public void setThreadNo2b(String threadNo2b) {
        this.threadNo2b = threadNo2b;
    }

    public String getThreadNo3() {
        return threadNo3;
    }

    public void setThreadNo3(String threadNo3) {
        this.threadNo3 = threadNo3;
    }
}
