package com.example;

import com.example.data.EndOfDay;
import com.example.domain.EndOfDayCalculationTask;
import com.example.domain.EndOfDayFileProcessorServiceImpl;
import com.example.domain.FileProcessorService;
import com.example.helper.EndOfDayMapperImpl;
import com.example.helper.MapperHelper;

public class HijraBankApplication {
    public static void main(String[] args) throws Exception {
        MapperHelper<EndOfDay, String> mapper = new EndOfDayMapperImpl();
        FileProcessorService<EndOfDay> fileProcessorService = new EndOfDayFileProcessorServiceImpl(mapper);
        EndOfDayCalculationTask task = new EndOfDayCalculationTask(fileProcessorService);
        task.execute();
    }
}
