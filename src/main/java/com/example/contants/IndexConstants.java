package com.example.contants;

public class IndexConstants {
    public static final int ID = 0;
    public static final int NAME = 1;
    public static final int AGE = 2;
    public static final int BALANCED = 3;
    public static final int PREVIOUS_BALANCED = 4;
    public static final int AVERAGE_BALANCED = 5;
    public static final int FREE_TRANSFER = 6;
}
