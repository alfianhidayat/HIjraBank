package com.example.contants;

public class HeaderConstants {
    public static final String ID = "id";
    public static final String NAME = "Name";
    public static final String AGE = "Age";
    public static final String BALANCED = "Balanced";
    public static final String PREVIOUS_BALANCED = "Previous Balanced";
    public static final String AVERAGE_BALANCED = "Average Balanced";
    public static final String FREE_TRANSFER = "Free Transfer";
    public static final String THREAD_1 = "No 1 Thread-No";
    public static final String THREAD_2A = "No 2a Thread-No";
    public static final String THREAD_2B = "No 2b Thread-No";
    public static final String THREAD_3 = "No 3 Thread-No";
}
