package com.example.contants;

public class FileConstants {

    public static final String BATH_PATH = "src/main/resources";
    public static final String BEFORE_EOD_FILE_PATH = BATH_PATH + "/Before Eod.csv";
    public static final String AFTER_EOD_FILE_PATH = BATH_PATH + "/After Eod.csv";
    public static final String SEMICOLON = ";";

}
